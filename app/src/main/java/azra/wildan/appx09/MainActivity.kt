package azra.wildan.appx09

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener {

    var posVidSkrg = 0
    lateinit var mediaController: MediaController
    lateinit var db : SQLiteDatabase
    lateinit var lsAdapter : ListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db = DBOpenHelper(this).writableDatabase
        mediaController = MediaController(this)
        mediaController.setPrevNextListeners(nextVid,prevVid)
        mediaController.setAnchorView(videoView2)
        mediaController.setAnchorView(videoView2)
        videoView2.setMediaController(mediaController)
        lv.setOnItemClickListener(itemClick)
    }

    fun showDataVid(){
        val cursor : Cursor = db.query("video", arrayOf("id_video as _id","id_cover","title"),
            null,null,null,null,"title asc")
        lsAdapter = SimpleCursorAdapter(this,R.layout.item_data_video,cursor,
            arrayOf("_id","id_cover","title"), intArrayOf(R.id.txIdVideo, R.id.txIdCover, R.id.txVideoTitle),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lv.adapter = lsAdapter
    }

    override fun onStart() {
        super.onStart()
        showDataVid()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        txJudulVideo.setText("Now Playing : "+c.getString(c.getColumnIndex("title")))
        imV.setImageResource(c.getInt(c.getColumnIndex("id_cover")))
        videoView2.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+c.getInt(c.getColumnIndex("_id"))))
    }

    var nextVid = View.OnClickListener { v:View ->
        if(posVidSkrg<(lv.adapter.count-1)) posVidSkrg++
        else posVidSkrg = 0
        videoSet(posVidSkrg)
    }

    var prevVid = View.OnClickListener { v:View ->
        if(posVidSkrg>0) posVidSkrg--
        else posVidSkrg = lv.adapter.count-1
        videoSet(posVidSkrg)
    }

    fun videoSet(pos: Int){
        val c: Cursor = lv.adapter.getItem(pos) as Cursor
        var id_cover = c.getInt(c.getColumnIndex("id_cover"))
        var title = c.getString(c.getColumnIndex("title"))
        videoView2.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+c.getInt(c.getColumnIndex("_id"))))
        imV.setImageResource(id_cover)
        txJudulVideo.setText(title)
    }

    override fun onClick(v: View?) {
        when(v?.id){
//            R.id.btnPlay ->{
//                audioPlay(posLaguSkrg)
//            }
//            R.id.btnNext ->{
//                audioNext()
//            }
//            R.id.btnPrev ->{
//                audioPrev()
//            }
//            R.id.btnStop ->{
//                audioStop()
//            }
        }
    }
}
